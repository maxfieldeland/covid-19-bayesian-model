#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 09:27:26 2020

@author: max

Max's initial efforts at checking out googles mobility dataset

"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


gmr = pd.read_csv('Global_Mobility_Report.csv')
print(gmr.columns)
print(gmr.dtypes)
sub = gmr.head(30)

# subset the US data 
gmr_us = gmr[gmr['country_region_code'] == 'US']
sub_us = gmr_us.head(50)

# subset the state level data
missing_table = gmr_us.isnull().sum()


# subset the data to just the state level aggregates

gmr_states = gmr_us[gmr_us['sub_region_2'].isnull()]
gmr_states = gmr_states[gmr_states['sub_region_1'].isnull() == False]
gmr_states.isnull().sum()
# working at the state level, there is not missingness
# save to csv
gmr_states.to_csv('gmr_us_states.csv')

# --------------------------------- Analysis ---------------------------------

