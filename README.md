# covid-19-bayesian-model
This repository contains all data and modelling scripts for Challange #3 from the UVM Data Labs Course. 

.
|-- us-counties.csv                 # Static COVID - 19 Timeseries at county level

|-- us-state.csv                    # Static COVID - 19 Timeseries at state level

|-- Global_Mobility_Report.csv      # Raw Google mobility report containing, all countries, states and counties

|-- gmr_us_states.csv               # Cleaned timeseries mobility data at aggregated at the state level

|-- prepare_gmr_data.py             # script to clean Global_Mobility_Report.csv

