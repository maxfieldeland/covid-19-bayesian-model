/* user defined functions. This is where we define our SIR model */
functions {
  real[] sir(real t, real[] y, real[] theta, real[] x_r, int[] x_i) {
    real dydt[2];
    dydt[1] = theta[1] * (theta[3] - y[2] - y[1]) * y[1] - theta[2] * y[1];
    dydt[2] = theta[2] * y[1];
    return dydt;
  }
}


/* the data you are assumed given. Defined externally and passed to stan */
data {
  int<lower = 0> N; // number of wards/counties/states
  int<lower = 0> K; // number of covariates
  int<lower = 0> P[N]; // pop. per ward
  int<lower = 0> S[N]; // number of days since first case per ward
  int<lower = 0> M; // total observation points (= sum(N * S))
  int<lower = 0> Y[M]; // obs. active cases
  int<lower = 0> T[M]; // test per ward
  /* matrix[N, K] X; // covariates per ward */

  real<lower = 0> mean_gamma;
  real<lower = 0> sd_gamma;
}

/* this is just a convenient place to put unused variables needed to appease the integrator */
transformed data {
  real x_r[0];
  int x_i[0];
}

/* model parameters which you are trying to infer */
parameters {
  real<lower = 0> gamma; // recovery rate
  /* vector[K] r; // slopes/contrasts of covariates */
  real<lower = 0, upper = 1> beta;
}

/* transformed parameters { */
/*   vector[N] beta = inv_logit(X * r); */
/* } */

/* the model: returns likelihood function L(parameters | data) */
model {
  int pos;
  int y0[3];
  int t0;
  real y_hat[M];
  real theta[3];
  y0 = [0, 1, 0];
  t0 = 1;
  gamma ~ normal(mean_gamma, sd_gamma);
  /* r ~ normal(0, 1);  */
  beta ~ uniform(0, 1);
  pos = 1;
  for (i in 1:N) {
    theta = [beta, gamma, P[i]];
    /* segment(ts, pos, S[i]) = 1:S[i]; */
    segment(y_hat, pos, S[i]) ~ col(integrate_ode_rk45(sir, y0, t0, 1:S[i], theta, x_r, x_i), 1);
    for (j in 0:(S[i]-1)) {
      Y[pos + j] ~ binomial(T[pos + j], y_hat[pos + j] / P[i]);
    }
    pos = pos + S[i];
  }
}

/* generated quantities { */
/*   vector[N] r = inv_logit(X * beta); */
/*   int y_pred[N] = bernoulli_rng(r); */
/* } */

